import type { Page, TestFixture } from '@playwright/test';
import { test } from '@playwright/test';
/**
 * Класс для реализации логики со страницей рейтинга
 *
 */
export class RatingPage {
  private page: Page;
  public errorSelector: string;
  public nThCounter: number;

  constructor({
    page,
   }: {
    page: Page;
  }) {
    this.page = page;
    this.nThCounter = 0;
    this.errorSelector = '.alertify-notifier';
  }

  get nextRatingSelector() {
    const nextLocator = this.page.locator('td[class*=rating-names_item-count].has-text-success').nth(this.nThCounter);
    this.nThCounter++;
    return nextLocator;
  }


  async openRatingPage() {
    return await test.step('Открываю страницу с рейтингом котиков', async () => {
      await this.page.goto('/rating')
    })
  }

  async mockAPIRequest() {
    this.page.route("*/**/api/likes/cats/rating", async route => {
        const request = route.request();
        const method = await request.method();
        const postData = await request.postDataJSON();

        await route.fulfill({
            status: 500,
            contentType: "application/json",
            body: JSON.stringify(Object.assign({ error: "Я куда то нажал и все пропало" }, postData)),
        });
    });
    }

}

export type RatingPageFixture = TestFixture<
  RatingPage,
  {
    page: Page;
  }
  >;

export const ratingPageFixture: RatingPageFixture = async (
  { page },
  use
) => {
  const ratingPage = new RatingPage({ page });

  await use(ratingPage);
};

