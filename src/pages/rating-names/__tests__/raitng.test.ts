import { test as base, expect } from '@playwright/test';
import { RatingPage, ratingPageFixture} from '../__page-object__'

const test = base.extend<{ ratingPage: RatingPage }>({
  ratingPage: ratingPageFixture,
});

test('При ошибке сервера в методе raiting - отображается попап ошибки', async ({ page, ratingPage   }) => {
  /**
   * Замокать ответ метода получения рейтинга ошибкой на стороне сервера
   * Перейти на страницу рейтинга
   * Проверить, что отображается текст ошибка загрузки рейтинга
   */
  await ratingPage.mockAPIRequest();
  await ratingPage.openRatingPage();
  await expect(page.locator(ratingPage.errorSelector)).toHaveText("Ошибка загрузки рейтинга");


});

test('Рейтинг котиков отображается', async ({ page, ratingPage   }) => {
  /**
   * Перейти на страницу рейтинга
   * Проверить, что рейтинг количества лайков отображается по убыванию
   */
  await ratingPage.openRatingPage();
  let currentLocator = await ratingPage.nextRatingSelector;
  while(currentLocator){
    const curNumber = Number(await currentLocator.textContent());
    const nextLocator = await ratingPage.nextRatingSelector;
    if (!nextLocator) break;
    const nextNumber = Number(await nextLocator.textContent());
    await expect(curNumber).toBeGreaterThanOrEqual(nextNumber);
    currentLocator = nextLocator;
  }


});
